﻿using System.Web;
using System.Web.Mvc;

namespace TestTask2._0_EasyPay
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
