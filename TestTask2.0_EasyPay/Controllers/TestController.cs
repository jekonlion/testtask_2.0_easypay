﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using DataBase;

namespace TestTask2._0_EasyPay.Controllers
{
    public class TestController : ApiController
    {
        public IEnumerable<SelectCategories> Get()
        {
            using (Entities entities = new Entities())
            {
                var selectedCategories = entities.SelectCategories.ToList();
                return selectedCategories;
            }
        }

        public IEnumerable<SelectProductsByCategory_Result> Get(int id)
        {
            if (id > 4)
                ModelState.AddModelError("", "");
            if (ModelState.IsValid)
            {
                using (Entities entities = new Entities())
                {

                    var products = entities.SelectProductsByCategory(id).ToList();
                    return products;
                }
            }
            return null;
        }

        public IEnumerable<AddProductToBasket_Result> Post(int id)
        {
            using (Entities entities = new Entities())
            {
                var products = entities.AddProductToBasket(id).ToList();
                return products;
            }
        }
    }
}
